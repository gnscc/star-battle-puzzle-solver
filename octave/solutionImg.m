clear all;
close all;
pkg load image
load ./mat/statement.mat
load ./mat/squaresBb.mat
starsText = fopen ("./txt/stars.txt");
stars =  fgetl(starsText);
fclose(starsText);

labeledStm = bwlabel(statement);
stars = strsplit(stars,',');
starsPoints = [];
starsLabels = [];
for i=1:size(stars)(2)
  idx = str2num(stars{i})
  starsPoints(end+1) = round(squaresBb(idx).Centroid(1));
  starsPoints(end+1) = round(squaresBb(idx).Centroid(2));
  starsLabels(end+1) = labeledStm(starsPoints(end),starsPoints(end-1))
end;

squaresNum = size(squaresBb)(1)
colors = repmat ([0 0 0], squaresNum, 1);

labelsNum = size(starsLabels)(2);
for i=1:labelsNum
  colors(starsLabels(i)) = 40;
  colors(starsLabels(i) + squaresNum) = 0;
  colors(starsLabels(i) + 2*squaresNum) = 100;
end;

negSolImgs = label2rgb(labeledStm, colors);

solution = imcomplement(negSolImgs);

imwrite(solution, './img/solution.png');