<?php
$starttime = microtime(true);

// Read statement with octave
exec('octave ./octave/processStatement.m');
/*$initialMap = [
    ['A','A','A','B','C','C','C','C','C','D'],
    ['A','E','A','B','C','C','D','D','D','D'],
    ['A','E','E','B','B','B','D','F','F','D'],
    ['A','E','E','E','B','B','D','F','F','F'],
    ['A','A','E','E','B','F','F','F','G','G'],
    ['A','A','E','B','B','H','H','H','G','G'],
    ['E','E','E','H','H','H','H','H','H','G'],
    ['E','I','J','J','J','G','H','H','G','G'],
    ['I','I','I','J','J','G','H','H','G','G'],
    ['I','J','J','J','J','G','G','G','G','G']
];*/

$preMap = str_split(file_get_contents('./txt/statement.txt'));
$stars = readline('How many stars per row, column and shape? ');
//$stars = 4;

$initialMap = [];
$n = sqrt(sizeof($preMap));
for($i = 0; $i < $n; $i++) {
    $aux = [];
    for ($j = 0; $j < $n; $j++) {
        $aux[] = $preMap[$i+$j*$n];
    }
    $initialMap[] = $aux;
}

$n = sizeof($initialMap);
$scripts = [];
$shapesIDs = [];
$g='_';

$constraintNumber = 1;

// Create variables and search for shapes IDS
for ($i = 0; $i < $n; $i++) {
    for($j = 0; $j < $n; $j++) {
        $scripts[] = "var x".$i.$g.$j." binary;";

        if (!in_array($initialMap[$i][$j], $shapesIDs)) {
            $shapesIDs[] = $initialMap[$i][$j];
        }
    }
}

// Constraints for rows and columns
for ($i = 0; $i < $n; $i++) {
    $rowScript = "c".$constraintNumber.": x".$i.$g."0";
    $colScript = "c".($constraintNumber+1).": x"."0".$g.$i;

    for($j = 1; $j < $n; $j++) {
        $rowScript = $rowScript." + x".$i.$g.$j;
        $colScript = $colScript." + x".$j.$g.$i;
    }
    $rowScript = $rowScript." = ".$stars.";";
    $colScript = $colScript." = ".$stars.";";


    $scripts[] = $rowScript;
    $scripts[] = $colScript;

    $constraintNumber++;
    $constraintNumber++;
}

// Constraints for shapes
$auxScripts = [];

for ($k = 0; $k < $n; $k++) {
    $auxScripts[] = "c".$constraintNumber.": 0";
    $constraintNumber++;
}

for ($i = 0; $i < $n; $i++) {
    
    for($j = 0; $j < $n; $j++) {

        $k = array_search($initialMap[$i][$j], $shapesIDs);
        $auxScripts[$k] = $auxScripts[$k]." + x".$i.$g.$j;
    }
}

for ($i = 0; $i < $n; $i++) {

    $auxScripts[$i] = $auxScripts[$i]." = ".$stars.";";
    $scripts[] = $auxScripts[$i];
}

// Constraints for espace (inequations)
for ($i = 0; $i < $n - 1; $i++) {
    for($j = 0; $j < $n - 1; $j++) {
        $script = "c".$constraintNumber.": x".$i.$g.$j." + x".($i+1).$g.$j." + x".$i.$g.($j+1)." + x".($i+1).$g.($j+1)." <= 1;";
        $scripts[] = $script;
        $constraintNumber++;
    }
}

// Solve
$scripts [] = 'solve;';
$scripts [] = 'end;';

// Create solver file
$ampl = fopen('solver.ampl','w');

for ($i = 0; $i < sizeof($scripts); $i++) {
    fwrite($ampl, $scripts[$i].PHP_EOL);
}

fclose($ampl);

// Execute script
exec('glpsol -m solver.ampl -w solver.sol');

// Solution to array
$manageFile = explode(" ",str_replace(PHP_EOL, " ", file_get_contents('solver.sol')));

// Print solution
$col = 0;
$solution = [];
$aux_row = [];
for ($i = 0; $i < sizeof($manageFile); $i++) {
    if ($manageFile[$i] == 'j') {
        echo($manageFile[$i+2]);
        $aux_row[] = $manageFile[$i+2];
        $col++;
        if ($col == $n) {
            echo(PHP_EOL);
            $solution[] = $aux_row;
            $aux_row = [];
            $col = 0;
        } else {
            echo(" ");
        }
    }
}

$string = '';
$count = 0;
for ($i = 0; $i < $n; $i++) {
    for ($j = 0; $j < $n; $j++) {
        if($solution[$j][$i] == 1) {
            $count++;
            $idx = $i*$n + $j + 1; 
            $string = $string.$idx;
            if ($count!= $n*$stars) {
                $string = $string.',';
            }
        }
    }
}

//var_dump($solution);
//var_dump($string);

$starsSol = fopen('./txt/stars.txt','w');
fwrite($starsSol, $string);
fclose($starsSol);

exec('octave ./octave/solutionImg.m');

$endtime = microtime(true);
$timediff = $endtime - $starttime;
echo($timediff.' s'.PHP_EOL);
?>